//
//  ForecastControllerTests.swift
//  ForecastAppTests
//
//  Created by Dmitry Poznukhov on 25.03.19.
//  Copyright © 2019 DmitryPoznukhov.com. All rights reserved.
//

import XCTest
@testable import ForecastApp

private let OneDayTimeInterval: TimeInterval = 60 * 60 * 24
private let forecast = Models.Forecast(list: [
    Models.ForecastEntity(dt: Date(), main: Models.ForecastEntityMain(temp: 0, tempMin: 0, tempMax: 0, pressureHpa: 0, humidity: 0), weather: [Models.Weather(main: "", description: "", icon: .clearSkyDay)], wind: Models.Wind(speed: 0, deg: 0)),
    Models.ForecastEntity(dt: Date().addingTimeInterval(OneDayTimeInterval), main: Models.ForecastEntityMain(temp: 0, tempMin: 0, tempMax: 0, pressureHpa: 0, humidity: 0), weather: [Models.Weather(main: "", description: "", icon: .clearSkyDay)], wind: Models.Wind(speed: 0, deg: 0)),
    Models.ForecastEntity(dt: Date().addingTimeInterval(OneDayTimeInterval * 2), main: Models.ForecastEntityMain(temp: 0, tempMin: 0, tempMax: 0, pressureHpa: 0, humidity: 0), weather: [Models.Weather(main: "", description: "", icon: .clearSkyDay)], wind: Models.Wind(speed: 0, deg: 0)),
    Models.ForecastEntity(dt: Date().addingTimeInterval(OneDayTimeInterval * 2), main: Models.ForecastEntityMain(temp: 0, tempMin: 0, tempMax: 0, pressureHpa: 0, humidity: 0), weather: [Models.Weather(main: "", description: "", icon: .clearSkyDay)], wind: Models.Wind(speed: 0, deg: 0))
    ]
)

class ForecastControllerTests: XCTestCase {

    private typealias MockForecastResult = NetworkResult<[String: String], Models.Error>
    private var networking = MockServicesNetworkingProvider()
    private var controller: Controllers.Forecast?

    override func setUp() {
        controller = Controllers.Forecast(environment: .localhost, networking: networking)
    }

    override func tearDown() {
        controller = nil
    }

    func testGetForecastSuccess() {
        networking.successfull = true


        var success: Bool? = nil
        _ = controller?.get(city: "TestCity", units: .metric) { result in success = result != nil }


        XCTAssert(success!)
    }

    func testGetForecastFailure() {
        networking.successfull = false


        var success: Bool? = nil
        _ = controller?.get(city: "TestCity", units: .metric) { result in success = result != nil }


        XCTAssert(!success!)
    }

    func testGroupForecast() {
        let grouped = controller?.groupForecastByDay(from: forecast)


        XCTAssert(grouped?.count == 3)
    }
}

private class MockServicesNetworkingProvider: ServicesNetworkingProvider {
    var successfull = false

    func jsonRequest<D: Decodable, E: Decodable>(_ request: URLRequest, completion: @escaping (NetworkResult<D, E>) -> ()) -> ServicesCancelable {
        if successfull {
            let data = try! JSONEncoder().encode(forecast)
            let object = try! JSONDecoder().decode(D.self, from: data)
            completion(.success(object))
        } else {
            completion(.failure(.invalidData))
        }

        return MockCancelable()
    }
}

private struct MockCancelable: ServicesCancelable {
    func cancel() {}
}
