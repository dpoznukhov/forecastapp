//
//  DateComponentsTests.swift
//  ForecastAppTests
//
//  Created by Dmitry Poznukhov on 26.03.19.
//  Copyright © 2019 DmitryPoznukhov.com. All rights reserved.
//

import XCTest
@testable import ForecastApp

class DateComponentsTests: XCTestCase {

    override func setUp() {
    }

    override func tearDown() {
    }

    func testDateComponents() {
        let date = Date(timeIntervalSince1970: 0)


        let components = date.components


        XCTAssert(components.year == 1970)
        XCTAssert(components.month == 1)
        XCTAssert(components.day == 1)
    }
}
