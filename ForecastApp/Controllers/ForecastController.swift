//
//  ForecastController.swift
//  ForecastApp
//
//  Created by Dmitry Poznukhov on 24.03.19.
//  Copyright © 2019 DmitryPoznukhov.com. All rights reserved.
//

import Foundation

private let appId = "dc9e0635f22af3d63861cb136c8fa05f"

extension Controllers {

    class Forecast {

        typealias Environment = Services.Environment
        typealias NetworkingProvider = ServicesNetworkingProvider
        typealias Units = Services.Forecast.Units
        typealias Cancelable = ServicesCancelable
        typealias ForecastEntity = Models.ForecastEntity
        typealias ForecastResult = NetworkResult<Models.Forecast, Models.Error>

        private let service: Services.Forecast

        init(environment: Environment, networking: NetworkingProvider) {
            service = .init(environment: environment, networking: networking, appId: appId)
        }
    }
}

extension Controllers.Forecast {

    func get(city: String, units: Units, completion: @escaping ([[ForecastEntity]]?) -> ()) -> Cancelable {
        return service.get(city: city, units: units) { (result: ForecastResult) in
            guard case let .success(forecast) = result else { return completion(nil) }
            completion(self.groupForecastByDay(from: forecast))
        }
    }

    func groupForecastByDay(from forecast: Models.Forecast) -> [[Models.ForecastEntity]] {
        let groupedForecast = Dictionary(grouping: forecast.list) { $0.dt.components.day }
        var result = [[Models.ForecastEntity]]()
        groupedForecast.keys.sorted().forEach { result.append(groupedForecast[$0] ?? []) }
        return result
    }
}
