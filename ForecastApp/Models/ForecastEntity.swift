//
//  ForecastEntity.swift
//  ForecastApp
//
//  Created by Dmitry Poznukhov on 24.03.19.
//  Copyright © 2019 DmitryPoznukhov.com. All rights reserved.
//

import Foundation

extension Models {
    struct ForecastEntity: Codable {
        let dt: Date
        let main: ForecastEntityMain
        let weather: [Weather]
        let wind: Wind
    }
}
