//
//  ForecastEntityMain.swift
//  ForecastApp
//
//  Created by Dmitry Poznukhov on 24.03.19.
//  Copyright © 2019 DmitryPoznukhov.com. All rights reserved.
//

import Foundation

extension Models {
    struct ForecastEntityMain: Codable {

        let temp: Double
        let tempMin: Double
        let tempMax: Double
        let pressureHpa: Double
        let humidity: Double

        enum CodingKeys: String, CodingKey {
            case temp
            case tempMin = "temp_min"
            case tempMax = "temp_max"
            case pressureHpa = "pressure"
            case humidity
        }
    }
}
