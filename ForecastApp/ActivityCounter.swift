//
//  ActivityCounter.swift
//  ForecastApp
//
//  Created by Dmitry Poznukhov on 24.03.19.
//  Copyright © 2019 DmitryPoznukhov.com. All rights reserved.
//

import UIKit

class ActivityCounter: NetworkActivityCounter {

    private var activeRequests = 0

    func startLoading() {
        activeRequests += 1
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
    }

    func finishLoading() {
        activeRequests -= 1
        guard activeRequests == 0 else { return }
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
    }
}
