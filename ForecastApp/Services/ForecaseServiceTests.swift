//
//  ForecaseServiceTests.swift
//  ForecastAppTests
//
//  Created by Dmitry Poznukhov on 25.03.19.
//  Copyright © 2019 DmitryPoznukhov.com. All rights reserved.
//

import XCTest
@testable import ForecastApp

class ForecaseServiceTests: XCTestCase {

    private typealias MockForecastResult = NetworkResult<[String: String], Models.Error>
    private var networking = MockServicesNetworkingProvider()
    private var service: Services.Forecast?

    override func setUp() {
        service = Services.Forecast(environment: .localhost, networking: networking, appId: "123456789")
    }

    override func tearDown() {
        service = nil
    }

    func testGetForecastSuccess() {
        networking.successfull = true


        var success: Bool? = nil
        _ = service?.get(city: "TestCity", units: .metric) { (result: MockForecastResult) in
            success = false
            if case .success = result { success = true }
        }


        XCTAssert(success!)
    }

    func testGetForecastFailure() {
        networking.successfull = false


        var success: Bool? = nil
        _ = service?.get(city: "TestCity", units: .metric) { (result: MockForecastResult) in
            success = false
            if case .success = result { success = true }
        }


        XCTAssert(!success!)
    }
}

private class MockServicesNetworkingProvider: ServicesNetworkingProvider {
    var successfull = false

    func jsonRequest<D: Decodable, E: Decodable>(_ request: URLRequest, completion: @escaping (NetworkResult<D, E>) -> ()) -> ServicesCancelable {
        if successfull {
            let data = try! JSONEncoder().encode(["Message": "Woho!!!"])
            let object = try! JSONDecoder().decode(D.self, from: data)
            completion(.success(object))
        } else {
            completion(.failure(.invalidData))
        }

        return MockCancelable()
    }
}

private struct MockCancelable: ServicesCancelable {
    func cancel() {}
}
