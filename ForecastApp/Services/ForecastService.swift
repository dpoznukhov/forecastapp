//
//  ForecastService.swift
//  ForecastApp
//
//  Created by Dmitry Poznukhov on 24.03.19.
//  Copyright © 2019 DmitryPoznukhov.com. All rights reserved.
//

import Foundation

extension Services {

    struct Forecast: Service {

        let environment: Environment
        let networking: ServicesNetworkingProvider
        let appId: String

        init(environment: Environment, networking: ServicesNetworkingProvider, appId: String) {
            self.environment = environment
            self.networking = networking
            self.appId = appId
        }

        func get<F: Decodable, E: Decodable>(city: String, units: Units, completion: @escaping (NetworkResult<F, E>) -> ()) -> ServicesCancelable {
            return networking.jsonRequest(constructRequest(with: environment, path: "forecast", parameters: ["units": units.rawValue, "q": city, "appid": appId]), completion: completion)
        }
    }
}

extension Services.Forecast {

    enum Units: String {
        case imperial
        case metric
        case `default`
    }
}
