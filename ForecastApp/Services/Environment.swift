//
//  Environment.swift
//  ForecastApp
//
//  Created by Dmitry Poznukhov on 24.03.19.
//  Copyright © 2019 DmitryPoznukhov.com. All rights reserved.
//

import Foundation

extension Services {

    enum Environment: String {
        case localhost = "http://localhost/"
        case production = "https://api.openweathermap.org/data/2.5/"

        static let allCases: [Environment] = [.production, .localhost]

        var title: String {
            switch self {
            case .production:
                return "Production"
            case .localhost:
                return "Localhost"
            }
        }

        var baseUrlString: String {
            switch self {
            case .localhost:
                return "file://" + Bundle.main.bundlePath + "/"
            default:
                return rawValue
            }
        }
    }
}
