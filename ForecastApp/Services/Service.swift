//
//  Service.swift
//  ForecastApp
//
//  Created by Dmitry Poznukhov on 24.03.19.
//  Copyright © 2019 DmitryPoznukhov.com. All rights reserved.
//

import Foundation

protocol Service {

    func constructRequest(with environment: Services.Environment, path: String, parameters: [String: String]) -> URLRequest
}

extension Service {

    func constructRequest(with environment: Services.Environment, path: String, parameters: [String: String]) -> URLRequest {
        var components = URLComponents(string: environment.baseUrlString + path)!
        var queryItems = [URLQueryItem]()
        parameters.forEach { queryItems.append(URLQueryItem(name: $0.key, value: $0.value)) }
        components.queryItems = queryItems
        return URLRequest(url: components.url!)
    }
}
