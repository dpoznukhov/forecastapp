//
//  ServiceTests.swift
//  ForecastApp
//
//  Created by Dmitry Poznukhov on 25.03.19.
//  Copyright © 2019 DmitryPoznukhov.com. All rights reserved.
//

import XCTest
@testable import ForecastApp

private struct MockService: Service {

}

class ServiceTests: XCTestCase {

    private var service: Service?

    override func setUp() {
        service = MockService()
    }

    override func tearDown() {
        service = nil
    }

    func testConstructRequest() {
        let request = service?.constructRequest(with: .localhost, path: "test", parameters: ["testParameter": "test"])

        XCTAssert(request!.url!.absoluteString.starts(with: Controllers.Forecast.Environment.localhost.baseUrlString))
        XCTAssert(request!.url!.description.hasSuffix("/test?testParameter=test"))
    }
}
