//
//  ServicesNetworkingProvider.swift
//  ForecastApp
//
//  Created by Dmitry Poznukhov on 24.03.19.
//  Copyright © 2019 DmitryPoznukhov.com. All rights reserved.
//

import Foundation

protocol ServicesNetworkingProvider {

    func jsonRequest<D: Decodable, E: Decodable>(_ request: URLRequest, completion: @escaping (NetworkResult<D, E>) -> ()) -> ServicesCancelable
}

protocol ServicesCancelable {
    func cancel()
}
