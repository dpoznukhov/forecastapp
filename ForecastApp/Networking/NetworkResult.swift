//
//  NetworkResult.swift
//  ForecastApp
//
//  Created by Dmitry Poznukhov on 24.03.19.
//  Copyright © 2019 DmitryPoznukhov.com. All rights reserved.
//

import Foundation

public enum NetworkResult<ResultType: Decodable, ErrorModel: Decodable> {
    case failure(NetworkError<ErrorModel>)
    case success(ResultType)
}
