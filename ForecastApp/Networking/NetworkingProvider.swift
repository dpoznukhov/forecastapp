//
//  NetworkingProvider.swift
//  ForecastApp
//
//  Created by Dmitry Poznukhov on 24.03.19.
//  Copyright © 2019 DmitryPoznukhov.com. All rights reserved.
//

import Foundation

private let decoder = { () -> JSONDecoder in
    let decoder = JSONDecoder()
    decoder.dateDecodingStrategy = .secondsSince1970
    return decoder
}()

class NetworkingProvider {

    let session: URLSession
    let queue: OperationQueue
    let activityCounter: NetworkActivityCounter

    init(session: URLSession, queue: OperationQueue = .main, activityCounter: NetworkActivityCounter) {
        self.session = session
        self.queue = queue
        self.activityCounter = activityCounter
    }

    func request<D: Decodable, E: Decodable>(_ request: URLRequest, completion: @escaping (NetworkResult<D, E>) -> ()) -> Cancelable {

        if request.url?.absoluteString.hasPrefix("file") ?? false {
            return requestFile(request, completion: completion)
        } else {
            return requestNetwork(request, completion: completion)
        }
    }

    private func requestNetwork<D: Decodable, E: Decodable>(_ request: URLRequest, completion: @escaping (NetworkResult<D, E>) -> ()) -> Cancelable {
        let dispatchedCompletion = dispatch(completion: completion)
        let activityCounter = self.activityCounter

        activityCounter.startLoading()

        let task = session.dataTask(with: request) { [weak self] data, response, error in
            guard let self = self else { return }
            activityCounter.finishLoading()

            if let error = error, let errorData = data {
                return dispatchedCompletion(.failure(self.errorCompletion(withData: errorData, error: error)))
            } else if let error = error {
                return dispatchedCompletion(.failure(.generic(error)))
            }
            guard let data = data, let response = response as? HTTPURLResponse else {
                return dispatchedCompletion(.failure(.invalidData))
            }
            guard case 200..<300 = response.statusCode else {
                let parsedError: NetworkError<E> = self.errorCompletion(withData: data, error: error)
                return dispatchedCompletion(.failure(parsedError))
            }

            do {
                let object = try decoder.decode(D.self, from: data)
                dispatchedCompletion(.success(object))
            } catch {
                dispatchedCompletion(.failure(.invalidResponse))
            }
        }
        task.resume()
        return Cancelable(task: task)
    }

    private func requestFile<D: Decodable, E: Decodable>(_ request: URLRequest, completion: @escaping (NetworkResult<D, E>) -> ()) -> Cancelable {
        let dispatchedCompletion = dispatch(completion: completion)
        queue.addOperation {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: request.url?.path ?? ""), options: .alwaysMapped)
                let object = try decoder.decode(D.self, from: data)
                dispatchedCompletion(.success(object))
            } catch {
                dispatchedCompletion(.failure(.invalidResponse))
            }
        }
        return Cancelable(task: nil)
    }

    private func errorCompletion<E: Decodable>(withData data: Data, error: Error?) -> NetworkError<E> {
        if let errorResponseModel = try? decoder.decode(E.self, from: data) {
            return .server(errorResponseModel)
        }
        guard let error = error else {
            return .invalidData
        }
        return .generic(error)
    }

    private func dispatch<T>(completion: @escaping ((T) -> ())) -> ((T) -> ()) {
        let queue = self.queue
        return { result in
            if queue == OperationQueue.current {
                return completion(result)
            }
            queue.addOperation {
                completion(result)
            }
        }
    }
}
