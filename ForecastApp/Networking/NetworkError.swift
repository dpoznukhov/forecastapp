//
//  NetworkError.swift
//  ForecastApp
//
//  Created by Dmitry Poznukhov on 24.03.19.
//  Copyright © 2019 DmitryPoznukhov.com. All rights reserved.
//

import Foundation

public enum NetworkError<Model> {
    case generic(Swift.Error)
    case invalidData
    case invalidResponse
    case server(Model)
}
