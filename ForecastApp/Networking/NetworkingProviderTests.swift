//
//  NetworkingProviderTests.swift
//  ForecastApp
//
//  Created by Dmitry Poznukhov on 26.03.19.
//  Copyright © 2019 DmitryPoznukhov.com. All rights reserved.
//

import XCTest
@testable import ForecastApp

class NetworkingProviderTests: XCTestCase {

    private typealias Result = NetworkResult<String, String>
    private let activityCounter = MockActivityCounter()
    private var provider: NetworkingProvider?

    override func setUp() {
        provider = NetworkingProvider(session: URLSession(configuration: .default), activityCounter: activityCounter)
    }

    override func tearDown() {
        provider = nil
    }

    func testActivityCounterCalls() {
        activityCounter.finishExpectedCount = 3
        let e = expectation(description: "Finish")
        activityCounter.complection = {
            e.fulfill()
        }


        _ = provider?.jsonRequest(URLRequest(url: URL(string: "https://localhost")!)) { (_: Result) in }
        _ = provider?.jsonRequest(URLRequest(url: URL(string: "https://localhost")!)) { (_: Result) in }
        _ = provider?.jsonRequest(URLRequest(url: URL(string: "https://localhost")!)) { (_: Result) in }


        XCTAssert(activityCounter.startCount == 3)
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssert(activityCounter.finishCount == 3)


        activityCounter.reset()
    }
}

private class MockActivityCounter: NetworkActivityCounter {

    var startCount = 0
    var finishCount = 0
    var finishExpectedCount = 3
    var complection: (() -> Void)?

    func startLoading() {
        startCount += 1
    }

    func finishLoading() {
        finishCount += 1

        guard finishExpectedCount == finishCount else { return }
        complection?()
    }


    func reset() {
        startCount = 0
        finishCount = 0
        complection = nil
    }
}
