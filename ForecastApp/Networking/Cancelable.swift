//
//  Cancelable.swift
//  ForecastApp
//
//  Created by Dmitry Poznukhov on 24.03.19.
//  Copyright © 2019 DmitryPoznukhov.com. All rights reserved.
//

import Foundation

struct Cancelable {

    let task: URLSessionTask?

    func cancel() {
        task?.cancel()
    }
}
