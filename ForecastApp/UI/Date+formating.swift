//
//  Date+formating.swift
//  ForecastApp
//
//  Created by Dmitry Poznukhov on 24.03.19.
//  Copyright © 2019 DmitryPoznukhov.com. All rights reserved.
//

import Foundation

extension Date {
    public func asString(for timeZone: TimeZone) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm dd,MM"
        formatter.timeZone = timeZone
        return formatter.string(from: self)
    }
}
