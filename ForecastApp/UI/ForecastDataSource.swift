//
//  DataSource.swift
//  ForecastApp
//
//  Created by Dmitry Poznukhov on 24.03.19.
//  Copyright © 2019 DmitryPoznukhov.com. All rights reserved.
//

import UIKit

final class ForecastDataSource: NSObject {
    let dataSources: [ForecastDayDataSource]

    init(_ forecast: [[Controllers.Forecast.ForecastEntity]]) {
        dataSources = forecast.map { ForecastDayDataSource($0) }
        super.init()
    }
}

extension ForecastDataSource: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSources.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let dataSource = dataSources[indexPath.row]
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ForecastDayCell", for: indexPath) as? ForecastDayCell else { return UITableViewCell() }
        cell.collectionView.dataSource = dataSource
        cell.collectionView.reloadData()
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
