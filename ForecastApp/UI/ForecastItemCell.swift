//
//  ForecastItemCell.swift
//  ForecastApp
//
//  Created by Dmitry Poznukhov on 24.03.19.
//  Copyright © 2019 DmitryPoznukhov.com. All rights reserved.
//

import UIKit

class ForecastItemCell: UICollectionViewCell {

    @IBOutlet private(set) var iconImageView: UIImageView!
    @IBOutlet private(set) var temperatureLabel: UILabel!
    @IBOutlet private(set) var timeLabel: UILabel!
}
