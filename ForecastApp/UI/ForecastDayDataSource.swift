//
//  ForecastDayDataSource.swift
//  ForecastApp
//
//  Created by Dmitry Poznukhov on 24.03.19.
//  Copyright © 2019 DmitryPoznukhov.com. All rights reserved.
//

import UIKit

final class ForecastDayDataSource: NSObject {
    let forecast: [Controllers.Forecast.ForecastEntity]

    init(_ forecast: [Controllers.Forecast.ForecastEntity]) {
        self.forecast = forecast
        super.init()
    }
}

extension ForecastDayDataSource: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return forecast.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let dayForecast = forecast[indexPath.row]
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ForecastItemCell", for: indexPath) as? ForecastItemCell else { return UICollectionViewCell() }

        cell.temperatureLabel.text = String(format: "%.1f", dayForecast.main.temp)
        cell.timeLabel.text = dayForecast.dt.asString(for: .current)
        cell.iconImageView.image = UIImage(named: dayForecast.weather.first?.icon.rawValue ?? "")

        return cell
    }
}
