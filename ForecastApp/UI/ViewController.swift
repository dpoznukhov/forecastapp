//
//  ViewController.swift
//  ForecastApp
//
//  Created by Dmitry Poznukhov on 24.03.19.
//  Copyright © 2019 DmitryPoznukhov.com. All rights reserved.
//

import UIKit

private let city = "Munich,de"

class ViewController: UIViewController {

    @IBOutlet private var environmentButton: UIButton!
    @IBOutlet private var tableView: UITableView!

    // only for this scale, in real app this mast be done in more generic way, then we can inject just a controller and not networking
    var environment = Controllers.Forecast.Environment.localhost {
        didSet {
            _ = view
            createControllers()
            updateData()
        }
    }
    var networking: Controllers.Forecast.NetworkingProvider? {
        didSet {
            _ = view
            createControllers()
            updateData()
        }
    }
    var currentRequest: Controllers.Forecast.Cancelable?

    private var forecastController: Controllers.Forecast?
    private var tableViewContent: UITableViewDataSource? {
        didSet {
            tableView.dataSource = tableViewContent
            tableView.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        environmentButton.setGlobal(title: "Switch Environment(\(environment.title))")
        tableView.estimatedRowHeight = UITableView.automaticDimension
    }

    private func createControllers() {
        guard let networking = networking else { return }
        forecastController = Controllers.Forecast(environment: environment, networking: networking)
    }

    private func updateData() {
        currentRequest?.cancel()
        tableViewContent = ForecastDataSource([])
        currentRequest = forecastController?.get(city: city, units: .metric) { forecast in
            guard let forecast = forecast else { return }
            self.tableViewContent = ForecastDataSource(forecast)
        }
    }

    @IBAction func ShowEnvironmentSwitch(_ sender: Any) {
        let alertController = UIAlertController(title: "Environment", message: nil, preferredStyle: .actionSheet)

        Controllers.Forecast.Environment.allCases.forEach { environment in
            alertController.addAction(UIAlertAction(title: environment.title, style: .default) { [weak self] _ in
                self?.environment = environment
                self?.environmentButton.setGlobal(title: "Switch Environment(\(environment.title))")
            })
        }

        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel) { _ in })

        present(alertController, animated: true)
    }
}

extension UIButton {
    public func setGlobal(title: String) {
        setTitle(title, for: .normal)
        setTitle(title, for: .highlighted)
        setTitle(title, for: .disabled)
        setTitle(title, for: .selected)
        setTitle(title, for: .focused)
        setTitle(title, for: .application)
        setTitle(title, for: .reserved)
    }
}
